@extends('layouts.app')

@section('content')
<div class="container">
    <div >
        @if (Auth::check())            
            <h1 class="text-center">Pedidos</h1>
            <hr>
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Producto</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Costo unitario</th>
                    <th scope="col">Total</th>
                    </tr>
                </thead>
                <tbody class="productos">
                    @foreach ($pedidos as $pedido)
                    <tr>
                    <th scope="row">{{$pedido->Id_pedido}}</th>
                    <td>{{$pedido->cliente->Nombre_cliente . ' '. $pedido->cliente->Apellidos_cliente}}</td>

                    @foreach ($pedido->producto as $producto)
                        <td><img src="{{$producto->Modelo_producto}}" alt=""></td>
                    @endforeach
                    
                    <td>{{$pedido->Descripcion_pedido}}</td>
                    <td>{{$pedido->Fecha_pedido}}</td>
                    <td>{{$pedido->Cantidad_producto}}</td>
                    <td>{{$pedido->Costo_unitario}}</td>
                    <td>{{$pedido->Total}}</td>
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
            {{$pedidos->links()}}   
                
            @else
            <div class="alert alert-danger">
                <h1>Debes estar logeado para ver el contenido</h1>
            </div>
                
        @endif
    </div>
</div>
@endsection
