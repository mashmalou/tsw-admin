@extends('layouts.app')

@section('content')
<h1 class="text-center">Agregar Producto</h1>
<hr>
<div class="container">
    <div>
        @if (Auth::check())
        <form action="{{route('productos.store')}} " method="POST" enctype="multipart/form-data">
            @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Nombre_producto">Nombre</label>
                        <input type="text" name="Nombre_producto" id="Nombre_producto" placeholder="Nombre del producto" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="Precio_producto">Precio</label>
                        <input type="text" name="Precio_producto" id="Precio_producto" placeholder="Precio del producto" class="form-control" placeholder="0">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="Modelo_producto">Modelo</label>
                        <input type="file" name="Modelo_producto" id="Modelo_producto" class="form-control-file">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Tipo_producto">Tipo</label>
                        <input type="text" name="Tipo_producto" id="Tipo_producto" placeholder="Tipo del producto" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Descripcion_producto">Descripcion</label>
                        <textarea name="Descripcion_producto" id="Descripcion_producto" class="form-control"></textarea>
                    </div>
                </div>
                <input type="submit" value="Guardar Producto"class="btn btn-outline-success">
            </div>
        </form>
        @else
            <div class="alert alert-danger">
                <h1>Debes estar logeado para ver el contenido</h1>
            </div>

        @endif
              
    </div>
</div>
@endsection
