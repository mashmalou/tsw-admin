@extends('layouts.app')

@section('content')
<h1 class="text-center">Productos</h1>
<hr>
<div class="container">
    <div>
        @if (Auth::check())
            @if (session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
    
        <a href="{{route('productos.create')}} " class="btn btn-outline-primary mb-3">Añadir producto</a>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Precio</th>
                <th scope="col">Modelo</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody class="productos">
                @foreach ($productos as $producto)
                <tr>
                <th scope="row">{{$producto->Id_producto}}</th>
                <td>{{$producto->Nombre_producto}}</td>
                <td>{{$producto->Descripcion_producto}}</td>
                <td>$ {{$producto->Precio_producto}}</td>
                <td><img class="img-fluid img-thumbnail" src="{{$producto->Modelo_producto}}" alt=""></td>
                <td>{{$producto->Tipo_producto}}</td>
                <td>
                    <a href="{{route('productos.edit',$producto->Id_producto)}} " class="btn btn-outline-info mb-3">Editar</a>
                    <form action="{{route('productos.destroy', $producto->Id_producto)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Eliminar" class="btn btn-outline-danger" onclick="return confirm('Seguro que desear eliminar este producto?')">
                        {{-- <a href="{{route('productos.destroy', $producto->Id_producto)}} " class="btn btn-outline-danger">Eliminar</a> --}}
                    </form>
                </td>
                </tr>
                    
                @endforeach
            </tbody>
            </table>
            {{$productos->links()}}   

        @else
            <div class="alert alert-danger">
                <h1>Debes estar logeado para ver el contenido</h1>
            </div>

        @endif
              
    </div>
</div>
@endsection
