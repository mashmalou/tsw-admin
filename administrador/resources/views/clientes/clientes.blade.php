@extends('layouts.app')

@section('content')
<div class="container">
    <div >
        @if (Auth::check())            
            <h1 class="text-center">Clientes</h1>
            <hr>
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                    <tr>
                    <th scope="row">{{$cliente->Id_cliente}}</th>
                    <td>{{$cliente->Nombre_cliente}}</td>
                    <td>{{$cliente->Apellidos_cliente}}</td>
                    <td>{{$cliente->Email_cliente}}</td>
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
            {{$clientes->links()}}   
                
            @else
            <div class="alert alert-danger">
                <h1>Debes estar logeado para ver el contenido</h1>
            </div>
                
        @endif
    </div>
</div>
@endsection
