<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey='Id_cliente';

    public function pedido()
    {
        return $this->hasMany('App\Pedido');
    }
}
