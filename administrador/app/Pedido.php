<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $primaryKey='Id_pedido';

    public function cliente()
    {
        return $this->belongsTo('App\Cliente','Id_cliente');
    }
    
    public function producto()
    {
        return $this->hasMany('App\Producto','Id_producto','Id_producto');
    }
}
