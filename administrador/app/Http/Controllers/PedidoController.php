<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;

class PedidoController extends Controller
{
    public function verPedidos(){
        $pedidos = Pedido::orderBy('Id_pedido','desc')->paginate(5);
        return view('pedidos.pedidos',compact('pedidos'));
    }

}
