<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::orderBy('Id_producto','desc')->paginate(5);
        return view('productos.productos',compact('productos'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto=new Producto();
        $request->validate([
            'Nombre_producto'=>'required|min:5|max:30',
            'Precio_producto'=>'required|numeric',
            'Tipo_producto'=>'required|min:5|max:30',
            'Modelo_producto'=>'mimes:jpeg,png',
            'Descripcion_producto'=>'required|min:5',
        ]);
        $producto->Nombre_producto=$request->input('Nombre_producto');
        $producto->Precio_producto=$request->input('Precio_producto');
        $producto->Tipo_producto=$request->input('Tipo_producto');
        $producto->Descripcion_producto=$request->input('Descripcion_producto');
        $producto->Fecha_registro=date('Y-m-d H:i:s');
        $modelo = $request->file('Modelo_producto');
        if ($modelo) {
            $modelo_path = time() . $modelo->getClientOriginalName();
            $modelo->move(public_path().'/images/productos/',$modelo_path);
            $producto->Modelo_producto = '/images/productos/'.$modelo_path;
        }
        $producto->save();
        return redirect()->route('productos.index')->with(array('message' => 'Producto añadido correctamente!'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        return view('productos.edit',compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        $request->validate([
            'Nombre_producto'=>'required|min:5|max:30',
            'Precio_producto'=>'required|numeric',
            'Tipo_producto'=>'required|min:5|max:30',
            'Modelo_producto'=>'mimes:jpeg,png',
            'Descripcion_producto'=>'required|min:5',
        ]);
        $producto->Nombre_producto=$request->input('Nombre_producto');
        $producto->Precio_producto=$request->input('Precio_producto');
        $producto->Tipo_producto=$request->input('Tipo_producto');
        $producto->Descripcion_producto=$request->input('Descripcion_producto');
        $producto->Fecha_registro=date('Y-m-d H:i:s');
        $modelo = $request->file('Modelo_producto');
        if ($modelo) {
            $modelo_path = time() . $modelo->getClientOriginalName();
            $modelo->move(public_path().'/images/productos/',$modelo_path);
            $producto->Modelo_producto = '/images/productos/'.$modelo_path;
        }
        $producto->save();
        return redirect()->route('productos.index')->with(array('message' => 'Producto actualizado correctamente!'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return redirect()->route('productos.index')->with(array('message' => 'El producto ha sido eliminado correctamente!'));

    }
}
