<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    public function verClientes(){
        $clientes = Cliente::orderBy('Id_cliente','desc')->paginate(5);
        return view('clientes.clientes',compact('clientes'));
    }
}
