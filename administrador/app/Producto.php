<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $primaryKey='Id_producto';

    public function pedido()
    {
        return $this->belongsTo('App\Pedido');
    }
}
