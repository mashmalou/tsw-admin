-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: tsw_tienda
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carrito`
--

DROP TABLE IF EXISTS `carrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carrito` (
  `Id_cliente` int(10) unsigned NOT NULL,
  `Id_producto` int(10) unsigned NOT NULL,
  `Cantidad_producto` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `carrito_id_cliente_foreign` (`Id_cliente`),
  KEY `carrito_id_producto_foreign` (`Id_producto`),
  CONSTRAINT `carrito_id_cliente_foreign` FOREIGN KEY (`Id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE,
  CONSTRAINT `carrito_id_producto_foreign` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrito`
--

LOCK TABLES `carrito` WRITE;
/*!40000 ALTER TABLE `carrito` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `Id_cliente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre_cliente` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Apellidos_cliente` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email_cliente` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password_cliente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id_cliente`),
  UNIQUE KEY `clientes_email_cliente_unique` (`Email_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Mario Alejandro','Dominguez Salgado','marioads@gmail.com','$2y$10$sB9fGFAW1/Zf6W0nqXFwA.3XoUFrxwf7JihBPGR3HI798OPlGfg.e',NULL,'2018-11-21 10:53:58','2018-11-21 10:53:58'),(2,'Miguel Angel','Feliz Gallardo','felix@gmail.com','$2y$10$/6gg8PNqAyjPDQJ8M8.YOO4sxpDmkCFeePf.FrI/AN/jes521O.TK',NULL,'2018-11-21 11:03:12','2018-11-21 11:03:12'),(3,'Amado','Carrillo','srcielos@gmail.com','$2y$10$Lt61KTfBx90ci36DrFYWO.ZhJSzNxJE.GIX4z1Mx2bYj5NvS0YLLe',NULL,'2018-11-21 11:04:10','2018-11-21 11:04:10'),(4,'Flipe','Ferras Gomez','ferras@outlook.com','$2y$10$QIgVJqy.VcGFtd.Ai99QVOhYomFDdeL8uwLzL2KVHJGEfsjVO9RG2',NULL,'2018-11-22 03:21:43','2018-11-22 03:21:43'),(7,'Mario Alejandro','Dominguez Salgado','chinito@gmail.com','$2y$10$KurO8CUN1BlOZgrHgotu6eD7oxSUjfSg.aorGKzaV7hiu.knqjud2',NULL,'2018-11-30 01:42:40','2018-11-30 01:42:40');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_11_21_043838_usuarios',1),(4,'2018_11_21_043957_productos',1),(5,'2018_11_21_044045_modelos',1),(6,'2018_11_21_044136_tallas',1),(7,'2018_11_21_044211_pedidos',1),(8,'2018_11_21_044254_carrito',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos`
--

DROP TABLE IF EXISTS `modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelos` (
  `Id_producto` int(10) unsigned NOT NULL,
  `Color_1` varchar(360) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Color_2` varchar(360) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Color_3` varchar(360) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_1` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_2` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_3` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_4` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_5` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_6` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_7` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_8` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_9` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `modelos_id_producto_foreign` (`Id_producto`),
  CONSTRAINT `modelos_id_producto_foreign` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos`
--

LOCK TABLES `modelos` WRITE;
/*!40000 ALTER TABLE `modelos` DISABLE KEYS */;
INSERT INTO `modelos` VALUES (1,'/images/productos/modelo1_c1.jpg','/images/productos/modelo1_c2.jpg','/images/productos/modelo1_c3.jpg','/images/productos/modelo1_c1_l1.jpg','/images/productos/modelo1_c1_l2.jpg','/images/productos/modelo1_c1_l3.jpg','/images/productos/modelo1_c2_l1.jpg','/images/productos/modelo1_c2_l2.jpg','/images/productos/modelo1_c2_l3.jpg','/images/productos/modelo1_c3_l1.jpg','/images/productos/modelo1_c3_l2.jpg','/images/productos/modelo1_c3_l3.jpg',NULL,NULL);
/*!40000 ALTER TABLE `modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos` (
  `Id_pedido` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Id_cliente` int(10) unsigned NOT NULL,
  `Id_producto` int(10) unsigned NOT NULL,
  `Descripcion_pedido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fecha_pedido` date NOT NULL,
  `Cantidad_producto` int(11) NOT NULL,
  `Costo_unitario` double(5,2) NOT NULL,
  `Total` double(6,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id_pedido`),
  KEY `pedidos_id_cliente_foreign` (`Id_cliente`),
  KEY `pedidos_id_producto_foreign` (`Id_producto`),
  CONSTRAINT `pedidos_id_cliente_foreign` FOREIGN KEY (`Id_cliente`) REFERENCES `clientes` (`Id_cliente`) ON DELETE CASCADE,
  CONSTRAINT `pedidos_id_producto_foreign` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `producto_completo`
--

DROP TABLE IF EXISTS `producto_completo`;
/*!50001 DROP VIEW IF EXISTS `producto_completo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `producto_completo` (
  `Id_producto` tinyint NOT NULL,
  `Nombre_producto` tinyint NOT NULL,
  `Descripcion_producto` tinyint NOT NULL,
  `Precio_producto` tinyint NOT NULL,
  `Modelo_producto` tinyint NOT NULL,
  `CH` tinyint NOT NULL,
  `M` tinyint NOT NULL,
  `L` tinyint NOT NULL,
  `G` tinyint NOT NULL,
  `Color_1` tinyint NOT NULL,
  `Color_2` tinyint NOT NULL,
  `Color_3` tinyint NOT NULL,
  `logo_1` tinyint NOT NULL,
  `logo_2` tinyint NOT NULL,
  `logo_3` tinyint NOT NULL,
  `logo_4` tinyint NOT NULL,
  `logo_5` tinyint NOT NULL,
  `logo_6` tinyint NOT NULL,
  `logo_7` tinyint NOT NULL,
  `logo_8` tinyint NOT NULL,
  `logo_9` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `Id_producto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre_producto` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Descripcion_producto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Precio_producto` double(5,2) NOT NULL,
  `Modelo_producto` varchar(360) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fecha_registro` date NOT NULL,
  `Tipo_producto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'POLO CUELLO COMBINADO','Polo de algodón de corte recto con cuello solapa y manga corta. Cierre frontal con botones. Detalle vivos en tapeta y cuello combinados.',499.00,'/images/productos/modelo1.jpg','2018-11-28','Normal',NULL,NULL),(2,'POLO CUELLO COMBINADO','Polo de cuello camisero combinado a contraste. Manga corta acabada en rib. Bajo con aberturas laterales. Cierre frontal con botones.',499.00,'/images/productos/modelo2.jpg','2018-11-28','Normal',NULL,NULL),(3,'POLO CUELLO COMBINADO','botones detalle vivos en tapeta y cuello combinados.',499.00,'/images/productos/modelo3.jpg','2018-11-28','Normal',NULL,NULL),(4,'POLO DE RAYAS','Polo de piqué de manga corta. Cierre con botones.',299.00,'/images/productos/modelo4.jpg','2018-11-28','Normal',NULL,NULL),(5,'POLO CANALÉ MUSCLE FIT','Jersey de canalé muscle fit con cuello tipo polo con botones y manga corta.',649.00,'/images/productos/modelo5.jpg','2018-11-28','Normal',NULL,NULL),(6,'POLO DETALLE','Polo de algodón de corte recto con cuello de pico y manga corta. Cierre frontal con botones detalle vivos en tapeta.',499.00,'/images/productos/modelo6.jpg','2018-11-28','Normal',NULL,NULL),(7,'POLO PIQUÉ BANDAS','Polo de piqué con bandas. Cuello solapa. Cierre con botones. Manga corta.',499.00,'/images/productos/modelo7.jpg','2018-11-28','Normal',NULL,NULL),(8,'POLO PIQUÉ','Polo de piqué de manga corta. Cierre con botones.',499.00,'/images/productos/modelo8.jpg','2018-11-28','Normal',NULL,NULL),(9,'POLO ESTAMPADO RIB','Polo de piqué con cuello camisero y manga corta acabados en rib con rayas a contraste. Cierre frontal con botones. Estampado con dibujos en terciopelo y lisos.',499.00,'/images/productos/especial1.jpg','2018-12-01','Especial',NULL,NULL),(10,'POLO ESTAMPADO FLORAL','Polo de cuello camisero en rib combinado a contraste y manga corta. Detalle estampación floral en delantero y espalda. Bajo con aberturas laterales. Cierre con botones en delantero.',499.00,'/images/productos/especial2.jpg','2018-12-01','Especial',NULL,NULL),(11,'POLO FLORES','Polo estampado de flores. Modelo de cuello clásico combinado y manga corta con rib a contraste. Cierre frontal con botones.',499.00,'/images/productos/especial3.jpg','2018-12-01','Especial',NULL,NULL),(12,'POLO PIQUÉ BORDADOS','Polo de piqué con bordados. Cuello solapa. Cierre con botones. Manga corta',499.00,'/images/productos/especial4.jpg','2018-12-01','Especial',NULL,NULL),(13,'POLO PIQUÉ','Polo de piqué con estampado frontal. Cuello con cierre de botones a contraste.',499.00,'/images/productos/especial5.jpg','2018-12-01','Especial',NULL,NULL),(14,'POLO CUELLO ESTAMPADO','Polo de piqué. Cuello estampado en contraste. Manga corta con vivo en rib. Cierre frontal con botones.',499.00,'/images/productos/especial6.jpg','2018-12-01','Especial',NULL,NULL),(15,'POLO ESTAMPADO RIB','Polo estampado de piqué con cuello camisero y manga corta acabados en rib con rayas a contraste. Cierre frontal con botones.',499.00,'/images/productos/especial7.jpg','2018-12-01','Especial',NULL,NULL),(16,'POLO ESTAMPACIÓN CAMUFLAJE','Polo de cuello solapa y manga corta con acabados combinados a contraste. Cierre frontal con botones.',499.00,'/images/productos/especial8.jpg','2018-12-01','Especial',NULL,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tallas`
--

DROP TABLE IF EXISTS `tallas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tallas` (
  `Id_producto` int(10) unsigned NOT NULL,
  `CH` int(11) NOT NULL,
  `M` int(11) NOT NULL,
  `L` int(11) NOT NULL,
  `G` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `tallas_id_producto_foreign` (`Id_producto`),
  CONSTRAINT `tallas_id_producto_foreign` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`Id_producto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tallas`
--

LOCK TABLES `tallas` WRITE;
/*!40000 ALTER TABLE `tallas` DISABLE KEYS */;
INSERT INTO `tallas` VALUES (1,6,11,4,3,NULL,NULL);
/*!40000 ALTER TABLE `tallas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Christopher','Leal Gomez','chris_t_o@hotmail.com','$2y$10$kIH5tKcU4oAzxWLAecyZ7uzqxcERLxfJlGxrWR5E4lvpMjsd7ELtG','2018-12-05 06:49:16','2018-12-05 06:49:16',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `producto_completo`
--

/*!50001 DROP TABLE IF EXISTS `producto_completo`*/;
/*!50001 DROP VIEW IF EXISTS `producto_completo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `producto_completo` AS select `productos`.`Id_producto` AS `Id_producto`,`productos`.`Nombre_producto` AS `Nombre_producto`,`productos`.`Descripcion_producto` AS `Descripcion_producto`,`productos`.`Precio_producto` AS `Precio_producto`,`productos`.`Modelo_producto` AS `Modelo_producto`,`tallas`.`CH` AS `CH`,`tallas`.`M` AS `M`,`tallas`.`L` AS `L`,`tallas`.`G` AS `G`,`modelos`.`Color_1` AS `Color_1`,`modelos`.`Color_2` AS `Color_2`,`modelos`.`Color_3` AS `Color_3`,`modelos`.`logo_1` AS `logo_1`,`modelos`.`logo_2` AS `logo_2`,`modelos`.`logo_3` AS `logo_3`,`modelos`.`logo_4` AS `logo_4`,`modelos`.`logo_5` AS `logo_5`,`modelos`.`logo_6` AS `logo_6`,`modelos`.`logo_7` AS `logo_7`,`modelos`.`logo_8` AS `logo_8`,`modelos`.`logo_9` AS `logo_9` from ((`productos` join `tallas` on((`productos`.`Id_producto` = `tallas`.`Id_producto`))) join `modelos` on((`productos`.`Id_producto` = `modelos`.`Id_producto`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-07  1:33:29
